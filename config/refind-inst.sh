#!/bin/bash

#set e      # "if...else" blokovi su beskorisni ako je postavljeno! NE KORISTITI!!!

# sve što je potrebno za instalaciju refind već imamo u triosefi.img
# dakle, prikopčamo triosefi.img pod /mnt, prekopiramo gde šta treba u target

mount -t auto -o loop /triosefi.img /mnt
cd /mnt
cp -r keys /boot/efi/
cd EFI
cp -r boot tools /boot/efi/EFI

# izađemo iz /mnt i otkačimo ga
cd / && umount /mnt

# zatim instaliramo efibootmgr i registrujemo refind u NVRAM
# instalacija efibootmgr, za slučaj da nema mreže, može da se odradi i kroz support folder u iso
# zajedno sa grub-efi paketima ?
apt update && apt install -y efibootmgr

while getopts "vd:p:" opt; do
	case $opt in
		v) VERBOSE="-v"
		   ;;
		d) DISK="$OPTARG"
		   ;;
		p) PARTITION="$OPTARG"
		   ;;
		*) echo "Unknown parameter: $OPTARG"
		   ;;
	esac
done

if [ ! -z "$VERBOSE" ]; then
	V="True"
else
	V="False"
fi

if [ -z "$DISK" ] && [ -z "$PARTITION" ] && [ ! -z "$VERBOSE" ]; then
	echo "--! efibootmgr called with no parameters ( verbose output on ), defaulting to /dev/sda !--"
	efibootmgr -c -l \\EFI\\boot\\grubx64.efi -L rEFInd "$VERBOSE"
elif [ -z "$DISK" ] && [ -z "$PARTITION" ] && [ -z "$VERBOSE" ]; then
	echo "--! efibootmgr called with no parameters, defaulting to /dev/sda !--"
	efibootmgr -c -l \\EFI\\boot\\grubx64.efi -L rEFInd "$VERBOSE"
elif [ ! -z "$DISK" ] && [ -z "$PARTITION" ]; then
	echo "--! efibootmgr called with: -d $DISK -p $PARTITION (VERBOSE=$V) !--"
	efibootmgr -d "$DISK" -c -l \\EFI\\boot\\grubx64.efi -L rEFInd "$VERBOSE"
elif [ -z "$DISK" ] && [ ! -z "$PARTITION" ]; then
	echo "--! efibootmgr called with: -d $DISK -p $PARTITION (VERBOSE=$V) !--"
	efibootmgr -p "$PARTITION" -c -l \\EFI\\boot\\grubx64.efi -L rEFInd "$VERBOSE"
elif [ ! -z "$DISK" ] && [ ! -z "$PARTITION" ]; then
	echo "--! efibootmgr called with: -d $DISK -p $PARTITION (VERBOSE=$V) !--"
	efibootmgr -d "$DISK" -p "$PARTITION" -c -l \\EFI\\boot\\grubx64.efi -L rEFInd "$VERBOSE"
fi

exit 0
